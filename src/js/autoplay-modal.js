// autoplay posts
const posts = [
    {
        id: 1,
        author: {
            link: 'javascript:;',
            name: 'Леонил Крафт',
            avatar: 'https://via.placeholder.com/100x100',
        },
        date: '12 февраля 2010, 21:00',
        content: `
            <p>
                Внезапно, диаграммы связей будут в равной степени предоставлены сами себе.
                Кстати, сторонники тоталит ыаы варизма в науке, которые представляют собой яркий
                пример континентально-европейского типа политической культуры, будут объединены
                в целые кластеры себе подобных.
            </p>
            <p>
                Внезапно, диаграммы связей будут в равной степени предоставлены сами себе.
                Кстати, сторонники тоталит ыаы варизма в науке, которые представляют собой яркий
                пример континентально-европейского типа политической культуры, будут объединены
                в целые кластеры себе подобных.
            </p>
        `,
        isFavorite: true,
    },
    {
        id: 2,
        author: {
            link: 'javascript:;',
            name: 'Александра Бойц',
            avatar: 'https://via.placeholder.com/100x100',
        },
        date: '12 февраля 2010, 20:00',
        content: `
            <audio controls><source src=""></audio>
        `,
        isFavorite: true,
    },
    {
        id: 3,
        author: {
            link: 'javascript:;',
            name: 'Александра Бойц',
            avatar: 'https://via.placeholder.com/100x100',
        },
        date: '12 февраля 2010, 20:00',
        content: `
            <img src="https://via.placeholder.com/1000x600">
            <p>
                Внезапно, диаграммы связей будут в равной степени предоставлены сами себе.
                Кстати, сторонники тоталит ыаы варизма в науке, которые представляют собой яркий
                пример континентально-европейского типа политической культуры, будут объединены
                в целые кластеры себе подобных.
            </p>
        `,
        isFavorite: false,
    },
];

function debounce(f, ms) {
    let isCooldown = false;

    return function () {
        if (isCooldown) return;
        f.apply(this, arguments);
        isCooldown = true;
        setTimeout(() => isCooldown = false, ms);
    };
}

function animate({timing, draw, duration, onStart = function () { }, onEnd = function () { }}) {
    onStart();

    const start = performance.now();

    requestAnimationFrame(function animate(time) {
        let timeFraction = (time - start) / duration;
        if (timeFraction > 1) timeFraction = 1;

        // вычисление текущего состояния анимации
        const progress = timing(timeFraction);

        draw(progress); // отрисовать её

        if (timeFraction < 1) {
            requestAnimationFrame(animate);
        } else {
            onEnd();
        }
    });
}

const modalAutoplayPosts = document.getElementById('autoplay-posts');

if (modalAutoplayPosts) {
    let currentPostIndex = 0;

    let timer = null;
    const DELAY = 15000; // 15sec

    const nextButton = modalAutoplayPosts.querySelector('.autoplay-posts__nav-next');
    const prevButton = modalAutoplayPosts.querySelector('.autoplay-posts__nav-prev');

    const readLoader = modalAutoplayPosts.querySelector('.post__loader span');

    const userAvatar = modalAutoplayPosts.querySelector('[data-post-user-avatar]');
    const userName = modalAutoplayPosts.querySelector('[data-post-user-name]');
    const date = modalAutoplayPosts.querySelector('[data-post-date]');
    const content = modalAutoplayPosts.querySelector('[data-post-content]');
    const favoriteButton = modalAutoplayPosts.querySelector('[data-post-favorite-button]');

    document.addEventListener('click', (e) => {
        if (!e.target.closest('[data-autoplay-posts]')) return;

        initAutoplayModal();
    });

    nextButton.addEventListener('click', nextPost);
    prevButton.addEventListener('click', prevPost);

    function initAutoplayModal() {
        $.fancybox.open({
            src: modalAutoplayPosts,
            type: 'inline',
            opts: {
                closeExisting: true,
                beforeShow: beforeShowModal,
                afterShow: showNavButtons,
                afterClose: afterCloseModal,
            },
        });
    }

    function beforeShowModal() {
        document.querySelector('.mobile-menu').mmApi.close();

        fillModalPost(posts[currentPostIndex]);

        startTimer();
    }

    function afterCloseModal() {
        clearModalPost();
        currentPostIndex = 0;
        modalAutoplayPosts.classList.remove('loading');
        hideNavButtons();
        stopTimer();
        readLoader.style.visibility = 'visible';
    }

    function fillModalPost(post) {
        userAvatar.setAttribute('href', post.author.link);
        userAvatar.firstElementChild.setAttribute('src', post.author.avatar);
        userAvatar.firstElementChild.setAttribute('alt', post.author.name);

        userName.innerHTML = post.author.name;

        date.innerHTML = post.date;

        content.innerHTML = '';
        content.insertAdjacentHTML('afterbegin', post.content);

        favoriteButton.setAttribute('aria-selected', post.isFavorite ? 'true' : 'false');
    }

    function clearModalPost() {
        userAvatar.setAttribute('href', '');
        userAvatar.firstElementChild.setAttribute('src', '');
        userAvatar.firstElementChild.setAttribute('alt', '');

        userName.innerHTML = '';

        date.innerHTML = '';

        content.innerHTML = '';
    }

    function nextPost() {
        stopTimer();

        const nextPost = posts[currentPostIndex + 1];

        if (nextPost !== undefined) {
            currentPostIndex++;
            fillModalPost(nextPost);
            startTimer();
        } else {
            modalAutoplayPosts.classList.add('loading');
            // здесь ajax запрос на получение следующего поста
            // $.get('/get-post/:id', (responsePost) => {
            //     modalAutoplayPosts.classList.remove('loading');
            //     posts.push(responsePost);
            //     currentPostIndex++;
            //     fillModalPost(responsePost);
            //     startTimer();
            // });
        }
    }

    function prevPost() {
        stopTimer();

        modalAutoplayPosts.classList.remove('loading');
        readLoader.style.visibility = 'hidden';

        const prevPost = posts[currentPostIndex - 1];

        if (prevPost !== undefined) {
            currentPostIndex--;
            fillModalPost(prevPost);
        }
    }

    function startTimer() {
        stopTimer();

        animate({
            duration: DELAY,
            timing(timeFraction) {
                return timeFraction;
            },
            draw: (progress) => {
                readLoader.style.width = `${progress * 100}%`;
            },
            onStart: () => {
                timer = setTimeout(nextPost, DELAY);
            },
        });
    }

    function stopTimer() {
        if (timer) {
            timer = clearTimeout(timer);
        }
    }

    function setPositionNavButtons() {
        const modalBoundingClientRect = modalAutoplayPosts.getBoundingClientRect();

        prevButton.style.left = `${modalBoundingClientRect.left - 10}px`;
        nextButton.style.left = `${modalBoundingClientRect.right + 10}px`;
    }

    const debounceSetPositionNavButtons = debounce(setPositionNavButtons, 300);

    function showNavButtons() {
        setPositionNavButtons();

        prevButton.style.opacity = 1;
        nextButton.style.opacity = 1;

        window.addEventListener('resize', debounceSetPositionNavButtons);
    }

    function hideNavButtons() {
        prevButton.style.opacity = 0;
        nextButton.style.opacity = 0;

        window.removeEventListener('resize', debounceSetPositionNavButtons);
    }
}
