import { animate } from "./helper";

document.addEventListener('click', (e) => {
	const targetDropdown = e.target.closest('.dropdown');

	if (!targetDropdown) return;

	e.preventDefault();

	const group = targetDropdown.dataset.dropdownGroups;
	let openedItemsInGroup = [];

	if (group) {
		openedItemsInGroup = document.querySelectorAll(`.dropdown[aria-expanded="true"][data-dropdown-groups=${group}]`);
	}

	if(targetDropdown.getAttribute('aria-expanded') === 'false') {
		const content = targetDropdown.querySelector('.dropdown-content');
		const height = content.scrollHeight;

		animate({
			duration: 250,
			timing(timeFraction) { return timeFraction; },
			draw: (progress) => { content.style.height = `${progress * height}px`; },
			onEnd: () => {
				targetDropdown.setAttribute('aria-expanded', 'true');
				content.style.height = 'auto';

				setTimeout(() => {
					const offsetTop = targetDropdown.querySelector('.dropdown-header').offsetTop;
	
					window.scrollTo({
						top: offsetTop,
						left: 0,
						behavior: "smooth"
					});
				}, 50);
			},
		});
	}

	if (openedItemsInGroup.length) {
		openedItemsInGroup.forEach((item) => {
			const content = item.querySelector('.dropdown-content');
			const height = content.scrollHeight;

			animate({
				duration: 250,
				timing(timeFraction) { return timeFraction; },
				draw: (progress) => { content.style.height = `${(1 - progress) * height}px`; },
				onStart: () => { item.setAttribute('aria-expanded', 'false'); },
			});
		});
	}
});