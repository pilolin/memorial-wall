// open chat
const chatOpenTrigger = document.getElementById('chat-open-trigger');
const chatCloseTrigger = document.getElementById('chat-close-trigger');
const chat = document.getElementById('chat');

if (chat) {
    chatOpenTrigger.addEventListener('click', openChat);
    chatCloseTrigger.addEventListener('click', closeChat);

    function openChat() {
        chat.setAttribute('aria-hidden', 'false');
        document.body.classList.add('chat-is-opened');
    }

    function closeChat() {
        chat.setAttribute('aria-hidden', 'true');
        document.body.classList.remove('chat-is-opened');
    }
}

// expanded text post
document.addEventListener('click', (e) => {
    if(!e.target.closest('.post-content')) return;

    const post = e.target.closest('.post');
    const button = post.querySelector('.post-content__more');
    const hiddenText = post.querySelector('.post-content__more + [aria-hidden="true"]');

    if(!hiddenText) return;

    button.remove();
    hiddenText.setAttribute('aria-hidden', 'false');
});


// sign in
const formsContainer = document.querySelector('.sign-in-forms');
const codeContainer = document.querySelector('.sign-in-confirm');

document.addEventListener('submit', (e) => {
    if(!e.target.closest('.sign-up-form')) return;
    e.preventDefault();
    formToCode();
});

document.addEventListener('click', (e) => {
    if(!e.target.closest('.sign-in-confirm .btn')) return;
    e.preventDefault();
    codeToForm();
});

function formToCode() {
    formsContainer.classList.remove('fadeInRight');
    codeContainer.classList.remove('fadeOutRight');

    formsContainer.classList.add('animated', 'fast', 'fadeOutLeft');
    codeContainer.classList.add('animated', 'fast', 'fadeInLeft');

    setTimeout(() => {
        codeContainer.querySelector('input').focus();
    }, 100);
}

function codeToForm() {
    formsContainer.classList.remove('fadeOutLeft');
    codeContainer.classList.remove('fadeInLeft');

    formsContainer.classList.add('animated', 'fast', 'fadeInRight');
    codeContainer.classList.add('animated', 'fast', 'fadeOutRight');

    codeContainer.querySelector('form').reset();
}
