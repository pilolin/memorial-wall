import { hideScroll, showScroll } from "./manageScrollDocument";

// open mobile search filter
const mobileTriggerFilter = document.querySelector('.search__mobile-trigger-filter');
const mobileCloseFilter = document.querySelector('.search__mobile-close-filter');
const mobileFilter = document.getElementById('search-form');

if(mobileTriggerFilter) {
    const openSearchFilter = () => {
        hideScroll();
        mobileFilter.classList.add('is-open');
    };

    window.openSearchFilter = openSearchFilter;

    const closeSearchFilter = () => {
        showScroll();
        mobileFilter.classList.remove('is-open');
    };

    window.closeSearchFilter = closeSearchFilter;

    mobileTriggerFilter.addEventListener('click', openSearchFilter);
    mobileCloseFilter.addEventListener('click', closeSearchFilter);
}