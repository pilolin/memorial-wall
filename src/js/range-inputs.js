import noUiSlider from "nouislider";

Element.prototype.initRange = function() {
    const sliderElement = this.querySelector('.range-slider');
    const inputMin = this.querySelector('input[name*="min"]');
    const inputMax = this.querySelector('input[name*="max"]');

    const min = +this.dataset.min;
    const max = +this.dataset.max;
    const step = this.dataset.step ? this.dataset.step : 1;
    const tooltips = (this.dataset.isTooltips !== undefined) ? [true, true] : null;

    if(!(min && max)) return;

    const minValue = inputMin.value ? +inputMin.value : min;
    const maxValue = inputMax.value ? +inputMax.value : max;

    noUiSlider.create(sliderElement, {
        range: { min, max },
        step,
        connect: true,
        start: [minValue, maxValue],
        tooltips,
        format: {
            to: (value) => parseFloat(value, 2),
            from: (value) => parseFloat(value, 2),
        }
    });

    const changeInputValue = (values, handle) => {
        const value = +values[handle];

        if (!handle) {
            inputMin.value = value;
        } else {
            inputMax.value = value;
        }

        removeError();
    };

    const removeError = () => {
        if (!sliderElement.closest('.form-item_has-error')) return;

        const inputContainer = sliderElement.closest('.form-item_has-error');
        const error = inputContainer.querySelector('.form-item__error');

        inputContainer.classList.remove('form-item_has-error');
        error.innerHTML = '';
    }

    sliderElement.noUiSlider.on('update', changeInputValue);

    inputMin.addEventListener('change', function (e) {
        sliderElement.noUiSlider.set([this.value, null]);

        removeError(e);
    });

    inputMax.addEventListener('change', function (e) {
        sliderElement.noUiSlider.set([null, this.value]);

        removeError(e);
    });

    this.classList.add('init');
}

document.addEventListener('DOMContentLoaded', () => {
    const elements = document.querySelectorAll('.form-item_range:not(.init)');

    [...elements].forEach((element) => {
        element.initRange();
    })
})