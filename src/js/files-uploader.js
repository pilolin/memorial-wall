class filesUploader {
    constructor(selector) {
        this.filesStore = {};

        this.init();
    }

    init() {
       this.preventDefaults();
       this.highlight();
       this.unhighlight();
       this.dropFiles();
       this.inputFiles();
       this.deleteFileOnButton();
    }

    preventDefaults() {
        const eventsName = ['dragenter', 'dragover', 'dragleave', 'drop'];

        eventsName.forEach((eventName) => {
            document.addEventListener(eventName, (e) => {
                if(!e.target.closest('.form-item_upload')) return;

                e.preventDefault()
                e.stopPropagation()
            }, false);
        });
    }

    highlight() {
        const eventsName = ['dragenter', 'dragover'];

        eventsName.forEach(eventName => {
            document.addEventListener(eventName, (e) => {
                const dropArea = e.target.closest('.form-item_upload');

                if(!dropArea) return;

                dropArea.classList.add('highlight');
            }, false);
        });
    }

    unhighlight() {
        const eventsName = ['dragleave', 'drop'];

        eventsName.forEach((eventName) => {
            document.addEventListener(eventName, (e) => {
                const dropArea = e.target.closest('.form-item_upload');

                if(!dropArea) return;

                dropArea.classList.remove('highlight');
            }, false);
        });
    }

    dropFiles() {
        document.addEventListener('drop', (e) => {
            const dropArea = e.target.closest('.form-item_upload');

            if(!dropArea) return;

            const input = dropArea.querySelector('input');
            const files = e.dataTransfer.files;

            this.addFiles(files, input);
        }, false);
    }

    inputFiles() {
        document.addEventListener('input', (e) => {
            if(!e.target.closest('.form-item_upload')) return;

            this.addFiles(e.target.files, e.target);
        });
    }

    deleteFileOnButton() {
        document.addEventListener('click', (e) => {
            const removeBtn = e.target.closest('.form-item_upload .form-item__files button');

            if(!removeBtn) return;

            const uploadBlock = e.target.closest('.form-item_upload');
            const input = uploadBlock.querySelector('input');
            const filesList = uploadBlock.querySelector('.form-item__files');
            const selectedFile = removeBtn.parentElement;
            const index = Array.prototype.indexOf.call(filesList.children, selectedFile);

            this.removeFileByIndex(input, index);
        }, false);
    }

    addFiles(files = [], input) {
        if(files.length && input) {
            if(!this.filesStore[input.name]) {
                this.filesStore[input.name] = input.multiple ? [] : null;
            }

            if(input.multiple) {
                [...files].forEach((file) => this.filesStore[input.name].push(file));
            } else {
                this.filesStore[input.name] = files[0];
            }

            this.renderFileList(input);
        }
    }

    removeFileByIndex(input, index = 0) {
        if(!input) return;

        this.filesStore[input.name].splice(index, 1);

        this.renderFileList(input);
    }

    getFiles(inputName, index = null) {
        if(!inputName) return undefined;

        const isMultiple = document.querySelector(`input[name="${inputName}"]`);

        if(isMultiple && index !== null) {
            return this.filesStore[inputName] && this.filesStore[inputName][index]
                ? this.filesStore[inputName][index]
                : null;
        } else {
            return this.filesStore[inputName] ? this.filesStore[inputName] : null
        }
    }

    renderFileList(input) {
        const uploadBlock = input.closest('.form-item_upload');
        const filesList = uploadBlock.querySelector('.form-item__files');

        if(filesList) {
            let newFiles;

            if(input.multiple) {
                newFiles = this.filesStore[input.name].map((file) => { return this.templateFileItemForRender(file) }).join('');
            } else {
                newFiles = this.templateFileItemForRender(this.filesStore[input.name]);
            }

            filesList.innerHTML = '';

            filesList.insertAdjacentHTML('afterbegin', newFiles);
        }
    }

    templateFileItemForRender(file) {
        return `
            <li>
                <svg><use width="16" height="16" xlink:href="#icon-clip"></svg>
                ${file.name}<span>${(file.size / 1024 / 1024).toFixed(2)}Мб</span>
                <button class="btn btn-icon">
                    <svg><use width="8" height="8" xlink:href="#icon-cancel-bold"></svg>
                </button>
            </li>
        `;
    }
}

window.filesUploader = new filesUploader();