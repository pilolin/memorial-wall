const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = [
	new HtmlWebpackPlugin({
		template: './src/index.pug',
		filename: './index.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/detail-page.pug',
		filename: './detail-page.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/sign-in-up.pug',
		filename: './sign-in-up.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/ui-kit.pug',
		filename: './ui-kit.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/search.pug',
		filename: './search.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/create-person.pug',
		filename: './create-person.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/modals-list.pug',
		filename: './modals-list.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/complaints.pug',
		filename: './complaints.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/profile-memory-list.pug',
		filename: './profile-memory-list.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/profile-messenger-dialog.pug',
		filename: './profile-messenger-dialog.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/profile-messenger-list.pug',
		filename: './profile-messenger-list.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/profile-notifications.pug',
		filename: './profile-notifications.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/profile-settings.pug',
		filename: './profile-settings.html'
	}),
	new HtmlWebpackPlugin({
		template: './src/main.pug',
		filename: './main.html'
	}),
    new HtmlWebpackPlugin({
        template: './src/condolences-people.pug',
        filename: './condolences-people.html'
    }),
]