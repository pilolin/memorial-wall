document.addEventListener('click', (e) => {
    if (!e.target.closest('[data-drop-menu-trigger]')) return;

    const trigger = e.target.closest('[data-drop-menu-trigger]');
    const targetId = trigger.dataset.dropMenuTrigger;

    if (targetId) {
        toggleMenu(document.getElementById(targetId));
    } else {
        toggleMenu(trigger.nextElementSibling);
    }
});

// close all
document.addEventListener('click', (e) => {
    if (e.target.closest('[data-drop-menu-trigger]')) return;

    const dropMenus = document.querySelectorAll('.drop-menu[aria-hidden="false"]');

    [...dropMenus].forEach((menu) => {
        menu.setAttribute('aria-hidden', 'true');
    });

    document.body.classList.remove('drop-menu-opened');
});

function toggleMenu(menu) {
    if(menu) {
        const isOpened = menu.getAttribute('aria-hidden') === 'false';

        menu.setAttribute('aria-hidden', isOpened ? 'true' : 'false');

        if (window.matchMedia('(max-width: 768px)').matches) {
            document.body.classList[isOpened ? 'remove' : 'add']('drop-menu-opened');
        }
    }
}