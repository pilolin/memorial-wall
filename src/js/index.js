import jquery from 'jquery';
import Inputmask from "inputmask";
import '@fancyapps/fancybox';
import './smoothScroll';
import './dropdown';
import './drop-menu';
import autosize from "autosize";
import "./range-inputs";
import "./search-filter";
import "./files-uploader";
import "./image-uploader";
import "./complaint";
import tippy from 'tippy.js';
import 'tippy.js/dist/tippy.css';
import 'tippy.js/themes/light.css';
import Swiper from 'swiper';
import 'swiper/swiper.scss';
import 'swiper/components/navigation/navigation.scss';
import 'swiper/components/pagination/pagination.scss';
import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper/core';
import Mmenu from 'mmenu-js'
import 'mmenu-js/src/mmenu.scss';
import './../scss/styles.scss';

window.$ = jquery;
window.jQuery = jquery;

tippy('[data-tippy-content]', {
    theme: 'light',
});

window.Mmenu = Mmenu;

SwiperCore.use([Navigation, Pagination, Autoplay]);

$('data-fancybox').fancybox();

autosize(document.querySelectorAll('textarea'));

// mask
new Inputmask('+7 (999) 999-99-99', { showMaskOnHover: false }).mask(document.querySelectorAll('input[type="tel"]'));

new Inputmask({
	mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{2,20}.*{2,6}[.*{1,2}]",
	greedy: false,
	showMaskOnHover: false,
	onBeforePaste: function (pastedValue) {
		pastedValue = pastedValue.toLowerCase();
		return pastedValue.replace("mailto:", "");
	},
	definitions: {
		'*': {
			validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
			casing: "lower"
		}
	}
}).mask(document.querySelectorAll('input.email-mask'));

// replace svg icons
window.addEventListener('load', () => {
	[...document.querySelectorAll('svg use')].forEach((element) => {
	    if (element.href.baseVal) {
            const targetSymbol = document.querySelector(element.href.baseVal);
            if (targetSymbol) {
                const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
                const viewBox = targetSymbol.viewBox.baseVal;

                svg.setAttributeNS(null,'viewBox', `${viewBox.x} ${viewBox.y} ${viewBox.width} ${viewBox.height}`);
                svg.setAttributeNS(null,'width', viewBox.width);
                svg.setAttributeNS(null,'height', viewBox.height);
                svg.setAttributeNS(null,'fill', 'none');
                svg.insertAdjacentHTML('afterbegin', targetSymbol.innerHTML);

                svg.classList.add(...element.parentElement.classList);

                element.parentElement.parentNode.replaceChild(svg, element.parentElement);
            }
        }
	});
});

document.addEventListener('DOMContentLoaded', () =>{
    setTimeout(() => {
        new Swiper('.slider-memory .swiper-container', {
            slidesPerView: 1,
            loop: true,
            autoplay: {
                delay: 5000,
                pauseOnMouseEnter: true,
            },
            navigation: {
                nextEl: '.slider-memory .swiper-button-next',
                prevEl: '.slider-memory .swiper-button-prev',
            },
            pagination: {
                el: '.swiper-pagination',
                type: 'bullets',
                dynamicBullets: true,
            },
            breakpoints: {
                450: {slidesPerView: 2},
                680: {slidesPerView: 4},
                768: {slidesPerView: 3},
                1200: {slidesPerView: 5}
            }
        });
    }, 200)
})

// sign in tabs
const tabsSignIn = document.querySelector('.sign-in-forms');
if(tabsSignIn) {
    tabsSignIn.addEventListener('click', (e) => {
        if(
            e.target.getAttribute('role') === 'tab'
            && e.target.getAttribute('aria-selected') === 'false'
        ) {
            e.preventDefault();

            const targetTab = e.target;
            const activeTab = tabsSignIn.querySelector('.sign-in-forms__tab[aria-selected="true"]');

            const targetContent = tabsSignIn.querySelector('.sign-in-forms__wrapper[aria-hidden="true"]');
            const activeContent = tabsSignIn.querySelector('.sign-in-forms__wrapper[aria-hidden="false"]');

            targetTab.setAttribute('aria-selected', "true");
            activeTab.setAttribute('aria-selected', "false");
            targetContent.setAttribute('aria-hidden', "false");
            activeContent.setAttribute('aria-hidden', "true");
        }
    })
}

document.addEventListener('input',  removeInputError);
document.addEventListener('change',  removeInputError);

function removeInputError(e) {
    if (!e.target.closest('.form-item_has-error')) return;

    const inputContainer = e.target.closest('.form-item_has-error');
    const error = inputContainer.querySelector('.form-item__error');

    inputContainer.classList.remove('form-item_has-error');
    error.innerHTML = '';
}

// next focus input in code form
const codeInputBlock = document.querySelector('.sign-in-confirm__code');
if (codeInputBlock) {
    codeInputBlock.addEventListener('input', (e) => {
        if(e.target.tagName.toLowerCase() === 'input') {
            const input = e.target;
            const parentInput = input.parentElement;

            if (parentInput.nextElementSibling) {
                parentInput.nextElementSibling.firstElementChild.focus();
            }
        }
    });
}

// open fixed-actions
const fixedActionsToggle = document.querySelector('.fixed-actions__toggle');
if (fixedActionsToggle) {
    fixedActionsToggle.addEventListener('click', (e) => {
        const list = document.querySelector('.fixed-actions__list');

        list.classList.toggle('opened');
    });
}