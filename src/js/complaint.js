import { animate } from "./helper";

document.addEventListener('click', (e) => {
    if(!e.target.classList.contains('complaint-post__header')) return;

    const post = e.target.parentElement;
    const isExpanded = post.getAttribute('aria-expanded') === 'true';

    const content = post.querySelector('.complaint-post__content');
    const heightContent = content.scrollHeight;

    const header = e.target;
    const headerText = header.firstElementChild;
    const currentText = headerText.innerHTML;
    const newText = header.dataset.text;

    if(isExpanded) {
        animate({
            duration: 250,
            timing(timeFraction) {
                return timeFraction;
            },
            draw: (progress) => {
                content.style.maxHeight = `${(1 - progress) * heightContent}px`;
            },
            onEnd: () => {
                post.setAttribute('aria-expanded', 'false');
                headerText.innerHTML = newText;
                header.dataset.text = currentText;
            }
        });
    } else {
        animate({
            duration: 250,
            timing(timeFraction) {
                return timeFraction;
            },
            draw: (progress) => {
                content.style.maxHeight = `${progress * heightContent}px`;
            },
            onEnd: () => {
                post.setAttribute('aria-expanded', 'true');
                content.style.maxHeight = 'auto';
                headerText.innerHTML = newText;
                header.dataset.text = currentText;
            },
        });
    }
});