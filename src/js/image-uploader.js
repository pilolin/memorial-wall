document.addEventListener('input', (e) => {
    if(!e.target.closest('.form-item_upload-img')) return;

    const file = e.target.files[0];
    const fileExtension = file.name.split('.').pop();

    if(file && ['png', 'jpeg', 'jpg', 'bmp', 'gif'].includes(fileExtension)) {
        const uploaderBlock = e.target.closest('.form-item_upload-img');
        const uploaderBackground = uploaderBlock.querySelector('.form-item__background');

        const fr = new FileReader();

        fr.onload = (e) => {
            uploaderBackground.style.backgroundImage = `url(${e.target.result})`;
        };

        fr.readAsDataURL(file);
    }
});